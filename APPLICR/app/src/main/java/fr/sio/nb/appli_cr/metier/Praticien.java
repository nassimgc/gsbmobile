package fr.sio.nb.appli_cr.metier;

public class Praticien {

    private int pra_num;
    private String pra_nom;
    private String pra_prenom;
    private String pra_adresse;
    private String pra_cp;
    private String pra_ville;
    private float pra_coefNotoriete;
    private String typ_code;
    private String vis_matricule;

    public Praticien(int pra_num,String pra_nom, String pra_prenom,String pra_adresse,
                     String pra_cp,String pra_ville,float pra_coefNotoriete,String typ_code,String vis_matricule)
    {
        this.pra_num = pra_num;
        this.pra_nom = pra_nom;
        this.pra_prenom = pra_prenom;
        this.pra_adresse = pra_adresse;
        this.pra_cp = pra_cp;
        this.pra_ville = pra_ville;
        this.pra_coefNotoriete = pra_coefNotoriete;
        this.typ_code = typ_code;
        this.vis_matricule = vis_matricule;
    }

    public int getPra_num() {
        return pra_num;
    }

    public void setPra_num(int pra_num) {
        this.pra_num = pra_num;
    }

    public String getPra_nom() {
        return pra_nom;
    }

    public void setPra_nom(String pra_nom) {
        this.pra_nom = pra_nom;
    }

    public String getPra_prenom() {
        return pra_prenom;
    }

    public void setPra_prenom(String pra_prenom) {
        this.pra_prenom = pra_prenom;
    }

    public String getPra_adresse() {
        return pra_adresse;
    }

    public void setPra_adresse(String pra_adresse) {
        this.pra_adresse = pra_adresse;
    }

    public String getPra_cp() {
        return pra_cp;
    }

    public void setPra_cp(String pra_cp) {
        this.pra_cp = pra_cp;
    }

    public String getPra_ville() {
        return pra_ville;
    }

    public void setPra_ville(String pra_ville) {
        this.pra_ville = pra_ville;
    }

    public float getPra_coefNotoriete() {
        return pra_coefNotoriete;
    }

    public void setPra_coefNotoriete(float pra_coefNotoriete) {
        this.pra_coefNotoriete = pra_coefNotoriete;
    }

    public String getTyp_code() {
        return typ_code;
    }

    public void setTyp_code(String typ_code) {
        this.typ_code = typ_code;
    }

    public String getVis_matricule() {
        return vis_matricule;
    }

    public void setVis_matricule(String vis_matricule) {
        this.vis_matricule = vis_matricule;
    }
}
