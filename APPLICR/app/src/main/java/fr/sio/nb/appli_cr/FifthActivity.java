package fr.sio.nb.appli_cr;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.sio.nb.appli_cr.metier.RapportVisite;
import fr.sio.nb.appli_cr.metier.TypeConfiance;
import fr.sio.nb.appli_cr.metier.TypePraticien;
import fr.sio.nb.appli_cr.metier.Visiteur;
import fr.sio.nb.appli_cr.modele.GsbSQLiteManager;

public class FifthActivity extends AppCompatActivity {
    private RapportVisite leRapport;
    private GsbSQLiteManager bdd;
    private TextView txtNum;
    private TextView txtNom;
    private TextView txtPrenom;
    private TextView txtAdresse;
    private TextView txtVille;
    private TextView txtCp;
    private TextView txtCoeff;
    private TextView txtCode;
    private TextView txtTypLibelle;
    private TextView txtNumRapport;
    private TextView txtConfiance;
    private TextView txtDateVisite;
    private TextView txtDateRapport;
    private TextView txtEtatRapport;
    private TextView txtBilan;
    private TextView txtMotif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth);

        txtNum=(TextView)findViewById(R.id.txtNum);
        txtNom = (TextView)findViewById(R.id.txtNom);
        txtPrenom = (TextView)findViewById(R.id.txtPrenom);
        txtAdresse = (TextView)findViewById(R.id.txtAdresse);
        txtVille = (TextView)findViewById(R.id.txtVille);
        txtCp = (TextView)findViewById(R.id.txtCp);
        txtCoeff = (TextView)findViewById(R.id.txtCoeff);
        txtCode = (TextView)findViewById(R.id.txtCode);
        txtTypLibelle = (TextView)findViewById(R.id.txtTypLibelle);
        txtNumRapport = (TextView)findViewById(R.id.txtNumRapport);
        txtConfiance = (TextView)findViewById(R.id.txtConfiance);
        txtDateVisite = (TextView)findViewById(R.id.txtDateVisite);
        txtDateRapport = (TextView)findViewById(R.id.txtDateRapport);
        txtEtatRapport = (TextView)findViewById(R.id.txtEtatRapport);
        txtMotif = (TextView)findViewById(R.id.txtMotif);
        txtBilan = (TextView)findViewById(R.id.txtBilan);

        Bundle paquet = this.getIntent().getExtras();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        String jsonRapport = paquet.getString("rapportChoisi");

        String leMatriculeVisiteur = paquet.getString("matriculeVisiteur");
        leRapport = gson.fromJson(jsonRapport, RapportVisite.class); // récupérer la chaine json pour le convertir en objet
        bdd = new GsbSQLiteManager(this);
        bdd.open();

            int numPraticien = leRapport.getLePraticien().getPra_num();
            String nomPraticien = leRapport.getLePraticien().getPra_nom();
            String prenomPraticien = leRapport.getLePraticien().getPra_prenom();
            String adressePraticien = leRapport.getLePraticien().getPra_adresse();
            String villePraticien = leRapport.getLePraticien().getPra_ville();
            String cpPraticien = leRapport.getLePraticien().getPra_cp();
            float coeffPraticien = leRapport.getLePraticien().getPra_coefNotoriete();

            int numRapport = leRapport.getRap_num();
            int idConfiance = leRapport.getIdConfiance();
            String typPraticien =  leRapport.getLePraticien().getTyp_code();
            String dateVisite = leRapport.getRap_dateVisite();
            String dateRapport = leRapport.getRap_dateRapport();
            String etatRapport = leRapport.getRap_etat();
            String motifRapport = leRapport.getRap_motif();
            String bilanRapport = leRapport.getRap_bilan();
            //String nomPraticien = bdd.getLeRapport(leMatriculeVisiteur,numRapport,numPraticien,dateVisite).getLePraticien().getPra_nom();
           // String prenomPraticien = bdd.getLeRapport(leMatriculeVisiteur,numRapport,numPraticien,dateVisite).getLePraticien().getPra_prenom();
            TypeConfiance laConfiance = bdd.getLaConfiance(idConfiance);
            String libelleConfiance = laConfiance.getLibelleConfiance();
            TypePraticien leTypePraticien = bdd.getLeTypePraticien(typPraticien);
            String codePraticien = leTypePraticien.getTyp_code();
            String libellePraticien = leTypePraticien.getTyp_libelle();
            String lieuPraticien = leTypePraticien.getTyp_lieu();



            txtNum.setText("Numéro : "+numPraticien);               txtNumRapport.setText("Rapport numéro : "+numRapport);
            txtNom.setText("Nom :"+nomPraticien);                   txtConfiance.setText("Confiance du praticien :"+libelleConfiance);
            txtPrenom.setText("Prenom :"+prenomPraticien);          txtDateVisite.setText("Date Visite : "+dateVisite);
            txtAdresse.setText("Adresse : "+adressePraticien);      txtDateRapport.setText("Date Rapport :"+dateRapport);
            txtVille.setText("Ville : "+villePraticien);            txtEtatRapport.setText("Etat du Rapport : "+etatRapport);
            txtCp.setText("Code Postal : "+cpPraticien);
            txtCoeff.setText("Coefficient de notoriété : "+coeffPraticien);
            txtCode.setText("Code Praticien : "+codePraticien);
            txtTypLibelle.setText("Métier : "+libellePraticien);


            txtMotif.setText("Motif :"+motifRapport);
            txtBilan.setText("Bilan : "+'\n'+bilanRapport);





    }
}
