package fr.sio.nb.appli_cr;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Calendar;

import fr.sio.nb.appli_cr.metier.Praticien;
import fr.sio.nb.appli_cr.metier.Visiteur;
import fr.sio.nb.appli_cr.modele.GsbSQLiteManager;

public class SixthActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private GsbSQLiteManager bdd = new GsbSQLiteManager(this);

    private Visiteur leVisiteur;
    private Spinner listePraticiens;
    private TextView leNom;
    private TextView laConfiance;
    private Spinner listeConfiance;
    private EditText txtMotif;
    private EditText txtBilan;

    private EditText txtDate;

    private int jourP;
    private int moisP;
    private int anneeP;
    int idPraticien;
    private DatePickerDialog picker;
    String vis_matricule;
    ArrayList<Praticien>lesPraticiens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sixth);

        txtDate = (EditText)findViewById(R.id.saisiDate);

        listePraticiens = (Spinner) findViewById(R.id.listePraticien);
        listeConfiance = (Spinner) findViewById(R.id.listeConfiance);

        leNom = (TextView)findViewById(R.id.leNom);
        laConfiance = (TextView)findViewById(R.id.laConfiance);

        txtMotif = (EditText)findViewById(R.id.motif);
        txtBilan = (EditText)findViewById(R.id.bilan);

        txtDate.setInputType(InputType.TYPE_NULL);
        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(SixthActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                jourP = dayOfMonth;
                                moisP = monthOfYear+1;
                                anneeP = year;
                                txtDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });


        Bundle paquet = this.getIntent().getExtras();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonVisiteur = paquet.getString("visiteurConnecter");

        leVisiteur = gson.fromJson(jsonVisiteur, Visiteur.class); // récupérer la chaine json pour le convertir en objet
        Log.i("ThirdAct::fromJson",leVisiteur.toString());



       vis_matricule = leVisiteur.getVisMatricule();
        bdd.open();


try {
    lesPraticiens = bdd.getLesPraticiens(vis_matricule);
    ArrayList<String> spinnerArray =  new ArrayList<String>();
    System.out.println("LA TAILLE DE LA LISTE" +lesPraticiens.size());
    for (Praticien p : lesPraticiens) {



        String nomPraticien = p.getPra_nom();
        System.out.println("oooooooooooh "+nomPraticien);
        spinnerArray.add(nomPraticien);
    }

    ArrayList<Integer>lesConfiances = bdd.getLesConfiances();
    ArrayAdapter<String> adaptateur = new ArrayAdapter<String>(
            this, android.R.layout.simple_spinner_item, spinnerArray
    );

    ArrayAdapter<Integer> adaptateur2 = new ArrayAdapter<Integer>(
            this, android.R.layout.simple_spinner_item, lesConfiances
    );
    adaptateur.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    adaptateur2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    listePraticiens.setAdapter(adaptateur);
    listeConfiance.setAdapter(adaptateur2);
    listeConfiance.setOnItemSelectedListener(this);
    listePraticiens.setOnItemSelectedListener(this);


}catch(Exception e){
    System.out.println("L'erreur "+e);

}




    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {

        Integer value2;


        switch(parent.getId())
        {
            case R.id.listeConfiance:
                value2 = (Integer) parent.getItemAtPosition(pos);
                laConfiance.setText(bdd.getLaConfiance(value2));

                break;
            case R.id.listePraticien:
                idPraticien =  ((Praticien) lesPraticiens.get(pos)).getPra_num();
                System.out.println("ID DU PRATICIEN SELECTIONNE "+idPraticien);
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public void enregistrer(View vue){

        String jour;
        String mois;
        String annee;
        bdd.open();

        int confianceSelectionne = (int) listeConfiance.getSelectedItem();

         jour = Integer.toString(jourP);
         moisP = moisP;
         mois = Integer.toString(moisP);
        annee = Integer.toString(anneeP);
        int anneeCourante = Calendar.getInstance().get(Calendar.YEAR);
        String motif = txtMotif.getText().toString();
        String bilan = txtBilan.getText().toString();
        String dateSaisi = txtDate.getText().toString();
        String dateVisite;

        if(bilan.isEmpty()||motif.isEmpty()||dateSaisi.isEmpty()|| anneeP>anneeCourante){
            Toast.makeText(this,"Un des éléments n'a pas été saisi correctement.", Toast.LENGTH_LONG).show();

        }else{

            try{
                if(jour.length()==1){
                    jour = "0"+jour;
                }
                if(mois.length()==1){
                    mois = "0"+mois;
                }
                dateVisite= annee+"-"+mois+"-"+jour;

                Log.i("Saisi rapport : ",dateVisite);
                Log.i("Saisi rapport2 : ",vis_matricule);

                bdd.addRapport(vis_matricule,idPraticien,dateVisite,confianceSelectionne,motif,bilan);

                Gson gson = new GsonBuilder().create();
                String jsonVisiteur = gson.toJson(leVisiteur); // transformer mon objet en chaine json
                Bundle paquet = new Bundle();
                paquet.putString("visiteurSaisi",jsonVisiteur);
                Intent intentEnvoyer = new Intent(this,SecondActivity.class);
                intentEnvoyer.putExtras(paquet);
                startActivity(intentEnvoyer);
            }catch(Exception e){
                System.out.println("L'erreur"+ e);
            }

        }


    }

    public void annuler(View vue) {

        txtDate.setText("");

        txtMotif.setText("");
        txtBilan.setText("");
        Toast.makeText(this,"La saisie a été annulée.", Toast.LENGTH_LONG).show();
    }

}
