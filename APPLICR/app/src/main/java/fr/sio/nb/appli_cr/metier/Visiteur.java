package fr.sio.nb.appli_cr.metier;

public class Visiteur {


    private String visMatricule;
    private String visNom;
    private String visPrenom;
    private String visAdresse;
    private String visCp;
    private String visVille;
    private String visLogin;
    private String visMdp;


    public Visiteur(String visMatricule, String visNom, String visPrenom, String visAdresse,
                    String visCp, String visVille, String visLogin,String visMdp) {
        this.visMatricule = visMatricule;
        this.visNom = visNom;
        this.visPrenom = visPrenom;
        this.visAdresse = visAdresse;
        this.visCp = visCp;
        this.visVille = visVille;
        this.visLogin = visLogin;
        this.visMdp = visMdp;
    }


    public String getVisMatricule() {
        return visMatricule;
    }

    public void setVisMatricule(String visMatricule) {
        this.visMatricule = visMatricule;
    }

    public String getVisNom() {
        return visNom;
    }

    public void setVisNom(String visNom) {
        this.visNom = visNom;
    }

    public String getVisPrenom() {
        return visPrenom;
    }

    public void setVisPrenom(String visPrenom) {
        this.visPrenom = visPrenom;
    }

    public String getVisAdresse() {
        return visAdresse;
    }

    public void setVisAdresse(String visAdresse) {
        this.visAdresse = visAdresse;
    }

    public String getVisCp() {
        return visCp;
    }

    public void setVisCp(String visCp) {
        this.visCp = visCp;
    }

    public String getVisVille() {
        return visVille;
    }

    public void setVisVille(String visVille) {
        this.visVille = visVille;
    }

    public String getVisLogin() {
        return visLogin;
    }

    public void setVisLogin(String visLogin) {
        this.visLogin = visLogin;
    }



    public String getVisMdp() {
        return visMdp;
    }

    public void setVisMdp(String visMdp) {
        this.visMdp = visMdp;
    }


    @Override
    public String toString() {
        return "Visiteur{" +
                "visNom='" + visNom + '\'' +
                ", visPrenom='" + visPrenom + '\'' +"visLogin "+visLogin+
                '}';
    }
}