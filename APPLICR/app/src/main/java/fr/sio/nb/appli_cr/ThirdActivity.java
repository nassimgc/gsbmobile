package fr.sio.nb.appli_cr;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import fr.sio.nb.appli_cr.metier.Visiteur;
import fr.sio.nb.appli_cr.modele.GsbSQLiteManager;

public class ThirdActivity extends AppCompatActivity {
    private Button lstMoisAnnee;
    private Button valider;
    private GsbSQLiteManager bdd = new GsbSQLiteManager(this);
    private Visiteur leVisiteur;
    private String monthYearStr;
    private SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
    private SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        lstMoisAnnee = (Button) findViewById(R.id.date);
        valider = (Button)findViewById(R.id.validerDate);
        bdd.open();

        lstMoisAnnee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthYearPickerDialog pickerDialog = new MonthYearPickerDialog();
                pickerDialog.setListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
                        monthYearStr = year + "-" + (month + 1) + "-" + i2;
                        lstMoisAnnee.setText(formatMonthYear(monthYearStr));
                    }
                });
                pickerDialog.show(getSupportFragmentManager(), "MonthYearPickerDialog");
            }
        });

        Bundle paquet = this.getIntent().getExtras();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonVisiteur = paquet.getString("visiteurConnecter");

        leVisiteur = gson.fromJson(jsonVisiteur, Visiteur.class); // récupérer la chaine json pour le convertir en objet
        Log.i("ThirdAct::fromJson",leVisiteur.toString());
    }

    public String formatMonthYear(String str) {
        Date date = null;
        try {
            date = input.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(date);
    }
    public void valider (View vue){

       // Toast.makeText(this,dateSelectionnee.substring(0,2), Toast.LENGTH_LONG).show(); mois
        //Toast.makeText(this,dateSelectionnee.substring(3,7), Toast.LENGTH_LONG).show(); annee

        if(lstMoisAnnee.getText().toString().equals("Choisir Date")){
            Toast.makeText(this,"Aucune date séléctionnée", Toast.LENGTH_LONG).show();
        }else{
            String dateSelectionnee =lstMoisAnnee.getText().toString();
            String mois = dateSelectionnee.substring(0,2);

            String annee = dateSelectionnee.substring(3,7);
            int m = Integer.parseInt(mois);

            Log.i("Mois Compte rendu: ",mois);
            Log.i("Annee Compte rendu: ",annee);
            int a = Integer.parseInt(annee);
System.out.println("Taille liste select les rapports"+bdd.getLesRapports(leVisiteur.getVisMatricule(),mois,a).size());
            if(bdd.getLesRapports(leVisiteur.getVisMatricule(),mois,a).size()>0){
                Gson gson = new GsonBuilder().create();
                String jsonVisiteur = gson.toJson(leVisiteur); // transformer mon objet en chaine json
                Bundle paquet = new Bundle();
                paquet.putString("visiteurConnecter2",jsonVisiteur);
                //String jsonDate = gson.toJson(dateSelectionnee);
                paquet.putString("dateSelectionnee",dateSelectionnee);

                Intent intentEnvoyer = new Intent(this,FourthActivity.class);
                intentEnvoyer.putExtras(paquet);
                startActivity(intentEnvoyer);
            }else{
                Toast.makeText(this,"Aucun rapport", Toast.LENGTH_LONG).show();
            }
        }
    }
}
