package fr.sio.nb.appli_cr.metier;

import java.util.ArrayList;
import java.util.Date;

public class RapportVisite {

    private int rap_num;
    private String vis_matricule;
    private int pra_num;
    private String rap_bilan;
    private String rap_dateVisite;
    private String rap_dateRapport;
    private String rap_motif;
    private int idConfiance;
    private String rap_etat;
    private Praticien lePraticien;

    public RapportVisite (int rap_num,String vis_matricule,int pra_num,String rap_bilan,
                          String rap_dateVisite,String rap_dateRapport,String rap_motif,
                          int idConfiance)
    {
        this.rap_num = rap_num;
        this.vis_matricule = vis_matricule;
        this.pra_num = pra_num;
        this.rap_bilan = rap_bilan;
        this.rap_dateVisite = rap_dateVisite;
        this.rap_dateRapport = rap_dateRapport;
        this.rap_motif = rap_motif;
        this.idConfiance = idConfiance;
        this.rap_etat = "lu";
    }

    public RapportVisite (int rap_num, String vis_matricule, int pra_num, String rap_bilan,
                          String rap_dateVisite, String rap_dateRapport, String rap_motif,
                          int idConfiance, Praticien lePraticien)
    {
        this.rap_num = rap_num;
        this.vis_matricule = vis_matricule;
        this.pra_num = pra_num;
        this.rap_bilan = rap_bilan;
        this.rap_dateVisite = rap_dateVisite;
        this.rap_dateRapport = rap_dateRapport;
        this.rap_motif = rap_motif;
        this.idConfiance = idConfiance;
        this.lePraticien = lePraticien;
        this.rap_etat = "lu";

    }

    public int getRap_num() {
        return rap_num;
    }

    public void setRap_num(int rap_num) {
        this.rap_num = rap_num;
    }

    public String getVis_matricule() {
        return vis_matricule;
    }

    public void setVis_matricule(String vis_matricule) {
        this.vis_matricule = vis_matricule;
    }

    public int getPra_num() {
        return pra_num;
    }

    public void setPra_num(int pra_num) {
        this.pra_num = pra_num;
    }

    public String getRap_bilan() {
        return rap_bilan;
    }

    public void setRap_bilan(String rap_bilan) {
        this.rap_bilan = rap_bilan;
    }

    public String getRap_dateVisite() {
        return rap_dateVisite;
    }

    public void setRap_dateVisite(String rap_dateVisite) {
        this.rap_dateVisite = rap_dateVisite;
    }

    public String getRap_dateRapport() {
        return rap_dateRapport;
    }

    public void setRap_dateRapport(String rap_dateRapport) {
        this.rap_dateRapport = rap_dateRapport;
    }

    public String getRap_motif() {
        return rap_motif;
    }

    public void setRap_motif(String rap_motif) {
        this.rap_motif = rap_motif;
    }

    public int getIdConfiance() {
        return idConfiance;
    }

    public void setIdConfiance(int idConfiance) {
        this.idConfiance = idConfiance;
    }

    public String getRap_etat() {
        return rap_etat;
    }

    public void setRap_etat(String rap_etat) {
        this.rap_etat = rap_etat;
    }

    public Praticien getLePraticien() {
        return lePraticien;
    }

    public void setLePraticien(Praticien lePraticien) {
        this.lePraticien = lePraticien;
    }
}
