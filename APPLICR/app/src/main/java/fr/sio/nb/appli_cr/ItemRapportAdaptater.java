package fr.sio.nb.appli_cr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.sio.nb.appli_cr.metier.RapportVisite;
import fr.sio.nb.appli_cr.modele.GsbSQLiteManager;

public class ItemRapportAdaptater extends ArrayAdapter<RapportVisite> {
    private GsbSQLiteManager bdd;
    private String matricule;
    private int annee;
    private String mois;


    public ItemRapportAdaptater (Context context, List<RapportVisite> lesEnseignants,String matricule,int annee,String mois) {
        super(context, -1, lesEnseignants );
        this.matricule = matricule;
        this.annee = annee;
        this.mois = mois;
        bdd = new GsbSQLiteManager(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vItem = null;
        bdd.open();
        // Si la vue est recyclée, elle contient déjà le bon layout
        if (convertView != null) {
            vItem = convertView ;
        } else { // il faut utiliser le LayoutInflater
            vItem = layoutInflater.inflate(R.layout.item_rapport, parent, false);
        }

        TextView tvNom = (TextView) vItem.findViewById(R.id.idTvDateVisite);
        String dateRapportAnglais = bdd.getLesRapports(matricule,mois,annee).get(position).getRap_dateVisite();
        String anneeF = dateRapportAnglais.substring(0,4);
        String moisF = dateRapportAnglais.substring(5,7);
        String  jourF = dateRapportAnglais.substring(8,10);
        String dateRapportFrancais = jourF+"/"+moisF+"/"+anneeF;
        tvNom.setText("Date visite : "+dateRapportFrancais);
        TextView tvPrenom = (TextView) vItem.findViewById(R.id.idTvNomPraticien);
        tvPrenom.setText("Praticien : "+bdd.getLesRapports(matricule,mois,annee).get(position).getLePraticien().getPra_nom());
        return vItem;
    }
}
