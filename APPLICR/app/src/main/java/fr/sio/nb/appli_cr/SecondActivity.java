package fr.sio.nb.appli_cr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import fr.sio.nb.appli_cr.metier.Visiteur;
import fr.sio.nb.appli_cr.modele.GsbSQLiteManager;
import fr.sio.nb.appli_cr.modele.GsbSQLiteOpenHelper;

public class SecondActivity extends AppCompatActivity {
    private TextView bienvenue;
    private TextView nomVisiteur;
    private Button consulterRapport;
    private Button saisirRapport;
    private Visiteur leVisiteur;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        bienvenue = (TextView)findViewById(R.id.bienvenue);
        nomVisiteur = (TextView)findViewById(R.id.nomVisiteur);
        consulterRapport = (Button)findViewById(R.id.consulter);
        saisirRapport = (Button)findViewById(R.id.saisir);




        Bundle paquet = this.getIntent().getExtras();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonVisiteur = paquet.getString("visiteurSaisi");

        leVisiteur = gson.fromJson(jsonVisiteur, Visiteur.class); // récupérer la chaine json pour le convertir en objet
        Log.i("SecondAct::fromJson",leVisiteur.toString());

        GsbSQLiteManager bdd = new GsbSQLiteManager(this);

        bdd.open();

        ArrayList<String> lesDates = new ArrayList<String>();
        lesDates = bdd.getDateRapport();


       // Prendre un élément d'une liste nomVisiteur.setText(lesDates.get(0));
        nomVisiteur.setText(leVisiteur.getVisNom()+" "+leVisiteur.getVisPrenom());

    }

    public void consulter (View vue){

        Gson gson = new GsonBuilder().create();

        String jsonVisiteur = gson.toJson(leVisiteur);
        Log.i("SecondAct::fromJson",leVisiteur.toString());// transformer mon objet en chaine json
        Bundle paquet = new Bundle();
        paquet.putString("visiteurConnecter",jsonVisiteur);
        Intent intentEnvoyer = new Intent(this,ThirdActivity.class);
        intentEnvoyer.putExtras(paquet);
        startActivity(intentEnvoyer);

    }

    public void saisir(View vue){

        Gson gson = new GsonBuilder().create();

        String jsonVisiteur = gson.toJson(leVisiteur);
        Log.i("SecondAct::fromJson",leVisiteur.toString());// transformer mon objet en chaine json
        Bundle paquet = new Bundle();
        paquet.putString("visiteurConnecter",jsonVisiteur);
        Intent intentEnvoyer = new Intent(this,SixthActivity.class);
        intentEnvoyer.putExtras(paquet);
        startActivity(intentEnvoyer);

    }

    public void deconnexion(View vue){


        Intent intentEnvoyer = new Intent(this,MainActivity.class);
        startActivity(intentEnvoyer);

    }
}
