package fr.sio.nb.appli_cr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.sio.nb.appli_cr.modele.*;
import fr.sio.nb.appli_cr.metier.*;
public class MainActivity extends AppCompatActivity {

    private EditText login;
    private EditText mdp;
    private Button btnConnexion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login =(EditText)findViewById(R.id.login);
        mdp =(EditText)findViewById(R.id.mdp);
        btnConnexion = (Button)findViewById(R.id.btnConnexion);


    }

    public void Connexion (View vue){
        Log.i("Main","debutClique");
        GsbSQLiteManager bdd = new GsbSQLiteManager(this);

        bdd.open();
        String loginVisiteur = login.getText().toString();
        String mdpVisiteur = mdp.getText().toString();
        Visiteur unVisiteur = bdd.getVisiteur(loginVisiteur,mdpVisiteur);

        if(unVisiteur != null){
            Gson gson = new GsonBuilder().create();
            String jsonVisiteur = gson.toJson(unVisiteur); // transformer mon objet en chaine json
            Bundle paquet = new Bundle();
            paquet.putString("visiteurSaisi",jsonVisiteur);
            Intent intentEnvoyer = new Intent(this,SecondActivity.class);
            intentEnvoyer.putExtras(paquet);
            startActivity(intentEnvoyer);

        }else{
            Toast.makeText(this,"Identifiants introuvables", Toast.LENGTH_LONG).show();
        }
        bdd.close();
    }
}
