package fr.sio.nb.appli_cr;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import fr.sio.nb.appli_cr.metier.RapportVisite;
import fr.sio.nb.appli_cr.metier.TypeConfiance;
import fr.sio.nb.appli_cr.metier.TypePraticien;
import fr.sio.nb.appli_cr.metier.Visiteur;
import fr.sio.nb.appli_cr.modele.GsbSQLiteManager;

public class FourthActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private Visiteur leVisiteur;
    private String laDate;
    private TextView dateAffichee;
    private GsbSQLiteManager bdd;
    private ListView lesRapports;
    String matricule;
    int m;
    int a;
    String mois;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);

        Bundle paquet = this.getIntent().getExtras();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        String jsonVisiteur = paquet.getString("visiteurConnecter2");

        String laDate = paquet.getString("dateSelectionnee");
        leVisiteur = gson.fromJson(jsonVisiteur, Visiteur.class); // récupérer la chaine json pour le convertir en objet
        bdd = new GsbSQLiteManager(this);
        bdd.open();
        matricule = leVisiteur.getVisMatricule();
         mois = laDate.substring(0,2);
        String annee = laDate.substring(3,7);
        m = Integer.parseInt(mois);
        a = Integer.parseInt(annee);
        dateAffichee = (TextView)findViewById(R.id.date);
        dateAffichee.setText("Les rapports du "+laDate);

        ArrayList<RapportVisite>lesRapportsEssai = bdd.getLesRapports(matricule,mois,a);


        for(RapportVisite unRapport : lesRapportsEssai){
            System.out.println("Le praticien "+unRapport.getRap_num());
        }
        lesRapports = (ListView)findViewById(R.id.lesRapports);
        ItemRapportAdaptater adapter = new ItemRapportAdaptater(this,
                bdd.getLesRapports(matricule,mois,a),matricule,a,mois);



        lesRapports.setAdapter(adapter);
        lesRapports.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        RapportVisite rapportSelectionne =  bdd.getLesRapports(matricule,mois,a).get(position);

        Gson gson = new GsonBuilder().create();
        String jsonRapport = gson.toJson(rapportSelectionne); // transformer mon objet en chaine json
        Bundle paquet = new Bundle();
        paquet.putString("rapportChoisi",jsonRapport);
        paquet.putString("matriculeVisiteur",matricule);
        Intent intentEnvoyer = new Intent(this,FifthActivity.class);
        intentEnvoyer.putExtras(paquet);
        startActivity(intentEnvoyer);
    }


}
