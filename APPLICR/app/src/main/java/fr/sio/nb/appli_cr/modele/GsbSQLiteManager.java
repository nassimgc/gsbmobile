package fr.sio.nb.appli_cr.modele;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.sio.nb.appli_cr.metier.Praticien;
import fr.sio.nb.appli_cr.metier.RapportVisite;
import fr.sio.nb.appli_cr.metier.TypeConfiance;
import fr.sio.nb.appli_cr.metier.TypePraticien;
import fr.sio.nb.appli_cr.metier.Visiteur;

public class GsbSQLiteManager {



    private static final String TABLE_VISITEUR = "visiteur";
    public static final String KEY_MATRICULE_VISITEUR="vis_matricule";
    public static final String KEY_NOM_VISITEUR="vis_nom";
    public static final String KEY_PRENOM_VISITEUR="vis_prenom";
    public static final String KEY_ADRESSE_VISITEUR="vis_adresse";
    public static final String KEY_CP_VISITEUR="vis_cp";
    public static final String KEY_VILLE_VISITEUR="vis_ville";
    public static final String KEY_LOGIN_VISITEUR="vis_login";
    public static final String KEY_MDP_VISITEUR="vis_mdp";

    private static final String TABLE_PRATICIEN = "praticien";
    private static final String KEY_NUMERO_PRATICIEN = "pra_num";
    private static final String KEY_NOM_PRATICIEN = "pra_nom";
    private static final String KEY_PRENOM_PRATICIEN = "pra_prenom";
    private static final String KEY_ADRESSE_PRATICIEN = "pra_adresse";
    private static final String KEY_CP_PRATICIEN = "pra_cp";
    private static final String KEY_VILLE_PRATICIEN = "pra_ville";
    private static final String KEY_NOTORIETE_PRATICIEN = "pra_coefNotoriete";
    private static final String KEY_TYPE_PRATICIEN = "typ_code";
    private static final String KEY_VISITEURMATRICULE_PRATICIEN = "vis_matricule";

    private static final String TABLE_TPRATICIEN = "type_praticien";
    private static final String KEY_CODE_TPRATICIEN = "typ_code";
    private static final String KEY_LIBELLE_TPRATICIEN = "typ_libelle";
    private static final String KEY_LIEU_TPRATICIEN = "typ_lieu";

    private static final String TABLE_TCONFIANCE = "type_confiance";
    private static final String KEY_IDCONFIANCE_TCONFIANCE = "idConfiance";
    private static final String KEY_LIBELLE_TCONFIANCE = "libelleConfiance";

    private static final String TABLE_RAPPORT = "rapport_visite";
    private static final String KEY_NUM_RAPPORT = "rap_num";
    private static final String KEY_VMATRICULE_RAPPORT = "vis_matricule";
    private static final String KEY_PRANUM_RAPPORT = "pra_num";
    private static final String KEY_BILAN_RAPPORT = "rap_bilan";
    private static final String KEY_DATEVISITE_RAPPORT = "rap_dateVisite";
    private static final String KEY_DATERAPPORT_RAPPORT = "rap_dateRapport";
    private static final String KEY_MOTIF_RAPPORT = "rap_motif";
    private static final String KEY_CONFIANCE_RAPPORT = "idConfiance";
    private static final String KEY_ETAT_RAPPORT = "rap_etat";


        private GsbSQLiteOpenHelper maBaseSQLite; // notre gestionnaire du fichier SQLite
        private SQLiteDatabase db;

        // Constructeur
    public GsbSQLiteManager(Context context)
        {
            maBaseSQLite = GsbSQLiteOpenHelper.getInstance(context);
        }

        public void open()
        {
            //on ouvre la table en lecture/écriture
            db = maBaseSQLite.getWritableDatabase();
        }

        public void close()
        {
            //on ferme l'accès à la BDD
            db.close();
        }

    public ArrayList<RapportVisite> getLesRapports1() {
        ArrayList<RapportVisite> lesRapports = new ArrayList<RapportVisite>();
        String $req = "SELECT * FROM praticien p join rapport_visite r on r.pra_num = p.pra_num WHERE  strftime('%m',rap_dateRapport) IN('03') AND strftime('%Y',rap_dateRapport) IN('2020')  ";

        Cursor c = db.rawQuery($req
                , null);
        int dd = c.getCount();
        System.out.println("Les rapports : "+$req);
        System.out.println("Les rapports Nb : "+dd);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            Praticien lePraticien = new Praticien(0,"","","","","",0.0f,"","");

            RapportVisite leRapport = new RapportVisite(0,"",0,"",null, null,"",0,null);

            lePraticien.setPra_num(c.getInt(c.getColumnIndex(KEY_NUMERO_PRATICIEN)));
            lePraticien.setPra_nom(c.getString(c.getColumnIndex(KEY_NOM_PRATICIEN)));
            lePraticien.setPra_prenom(c.getString(c.getColumnIndex(KEY_PRENOM_PRATICIEN)));
            lePraticien.setPra_adresse(c.getString(c.getColumnIndex(KEY_ADRESSE_PRATICIEN)));
            lePraticien.setPra_cp(c.getString(c.getColumnIndex(KEY_CP_PRATICIEN)));
            lePraticien.setPra_ville(c.getString(c.getColumnIndex(KEY_VILLE_PRATICIEN)));
            lePraticien.setPra_coefNotoriete(c.getFloat(c.getColumnIndex(KEY_NOTORIETE_PRATICIEN)));
            lePraticien.setTyp_code(c.getString(c.getColumnIndex(KEY_CODE_TPRATICIEN)));
            lePraticien.setVis_matricule(c.getString(c.getColumnIndex(KEY_VISITEURMATRICULE_PRATICIEN)));

            System.out.println("Les praticien requête: "+lePraticien.getPra_nom());

            leRapport.setRap_num(c.getInt(c.getColumnIndex(KEY_NUM_RAPPORT)));
            leRapport.setVis_matricule(c.getString(c.getColumnIndex(KEY_VMATRICULE_RAPPORT)));
            leRapport.setPra_num(c.getInt(c.getColumnIndex(KEY_PRANUM_RAPPORT)));
            leRapport.setRap_bilan(c.getString(c.getColumnIndex(KEY_BILAN_RAPPORT)));
            leRapport.setRap_dateVisite(c.getString(c.getColumnIndex(KEY_DATEVISITE_RAPPORT)));
            leRapport.setRap_dateRapport(c.getString(c.getColumnIndex(KEY_DATERAPPORT_RAPPORT)));
            leRapport.setRap_motif(c.getString(c.getColumnIndex(KEY_MOTIF_RAPPORT)));
            leRapport.setIdConfiance(c.getInt(c.getColumnIndex(KEY_CONFIANCE_RAPPORT)));
            leRapport.setRap_etat(c.getString(c.getColumnIndex((KEY_ETAT_RAPPORT))));
            leRapport.setLePraticien(lePraticien);




            lesRapports.add(leRapport);
            c.moveToNext();

        }


        c.close();


        return lesRapports;
    }





        public Visiteur getVisiteur(String login,String mdp) {
       /* String whereClause = "vis_matricule = ?";
        String[] whereArgs = new String[]{matricule};

        Cursor c= mDb.query("visiteur", new String[]{"*"},
                whereClause,
                whereArgs,
                null, null, null);
        Visiteur vis =new Visiteur("", "", "",
                "", "", "");*/
        Visiteur vis = new Visiteur("","","","","","","","");
        String sql ="SELECT * FROM visiteur WHERE vis_matricule";
        Cursor c = db.rawQuery("select * from " +TABLE_VISITEUR +
                " where " + KEY_LOGIN_VISITEUR + " = '" + login + "'  AND "+KEY_MDP_VISITEUR+" = '"+mdp+"'", null);

        Log.i("GsbSQLiteManager","requête");

        if(c.getCount()==0){
            vis =null;
        }else{
            if(c.moveToFirst()) {
                vis.setVisMatricule(c.getString(c.getColumnIndex(KEY_MATRICULE_VISITEUR)));
                vis.setVisNom(c.getString(c.getColumnIndex(KEY_NOM_VISITEUR)));
                vis.setVisPrenom(c.getString(c.getColumnIndex(KEY_PRENOM_VISITEUR)));
                vis.setVisAdresse(c.getString(c.getColumnIndex(KEY_ADRESSE_VISITEUR)));
                vis.setVisCp(c.getString(c.getColumnIndex(KEY_CP_VISITEUR)));
                vis.setVisVille(c.getString(c.getColumnIndex(KEY_VILLE_VISITEUR)));
                vis.setVisLogin(c.getString(c.getColumnIndex(KEY_LOGIN_VISITEUR)));
                c.close();
            }
        }
        ArrayList<RapportVisite> l = getLesRapports1();

        for(int i = 0; i < l.size();i++){
            System.out.println("Le rapport essai connexion "+i+" : "+l.get(i).getRap_dateRapport());
        }
        return vis;
    }


    public ArrayList<String> getDateRapport() {
        ArrayList<String> lesDates = new ArrayList<String>();
        String laDate = null;
        String dateEntiere=null;
        Cursor c = db.rawQuery("select " + KEY_DATERAPPORT_RAPPORT + "  from  " + TABLE_RAPPORT + " Order By "+ KEY_DATERAPPORT_RAPPORT +" Desc"
                , null);
        c.moveToFirst();
        while (!c.isAfterLast()) {

                    dateEntiere = c.getString(c.getColumnIndex(KEY_DATERAPPORT_RAPPORT));
                    String annee = dateEntiere.substring(0,4);
                    String mois = dateEntiere.substring(5,7);
                    laDate = mois+"/"+annee;

                    if(lesDates.contains(laDate)){
                        c.moveToNext();
                    }else{
                        lesDates.add(laDate);
                        c.moveToNext();
                    }
                }





        c.close();
        return lesDates;
    }

    public ArrayList<RapportVisite> getLesRapports(String vis_matricule,String mois,int annee) {
        ArrayList<RapportVisite> lesRapports = new ArrayList<RapportVisite>();
        String $req = "SELECT * FROM praticien p join rapport_visite r on r.pra_num = p.pra_num WHERE strftime('%m',rap_dateRapport) IN('"+mois+"') AND strftime('%Y',rap_dateRapport) IN('"+annee+"') AND r.vis_matricule = '"+vis_matricule+"'";

        Cursor c = db.rawQuery($req
                , null);
        int dd = c.getCount();
        System.out.println("Les rapports : "+$req);
        System.out.println("Les rapports Nb : "+dd);

            c.moveToFirst();
            while (!c.isAfterLast()) {
                Praticien lePraticien = new Praticien(0,"","","","","",0.0f,"","");

                RapportVisite leRapport = new RapportVisite(0,"",0,"",null, null,"",0,null);

                    lePraticien.setPra_num(c.getInt(c.getColumnIndex(KEY_NUMERO_PRATICIEN)));
                    lePraticien.setPra_nom(c.getString(c.getColumnIndex(KEY_NOM_PRATICIEN)));
                    lePraticien.setPra_prenom(c.getString(c.getColumnIndex(KEY_PRENOM_PRATICIEN)));
                    lePraticien.setPra_adresse(c.getString(c.getColumnIndex(KEY_ADRESSE_PRATICIEN)));
                    lePraticien.setPra_cp(c.getString(c.getColumnIndex(KEY_CP_PRATICIEN)));
                    lePraticien.setPra_ville(c.getString(c.getColumnIndex(KEY_VILLE_PRATICIEN)));
                    lePraticien.setPra_coefNotoriete(c.getFloat(c.getColumnIndex(KEY_NOTORIETE_PRATICIEN)));
                    lePraticien.setTyp_code(c.getString(c.getColumnIndex(KEY_CODE_TPRATICIEN)));
                    lePraticien.setVis_matricule(c.getString(c.getColumnIndex(KEY_VISITEURMATRICULE_PRATICIEN)));

                System.out.println("Les praticien requête: "+lePraticien.getPra_nom());

                    leRapport.setRap_num(c.getInt(c.getColumnIndex(KEY_NUM_RAPPORT)));
                    leRapport.setVis_matricule(c.getString(c.getColumnIndex(KEY_VMATRICULE_RAPPORT)));
                    leRapport.setPra_num(c.getInt(c.getColumnIndex(KEY_PRANUM_RAPPORT)));
                    leRapport.setRap_bilan(c.getString(c.getColumnIndex(KEY_BILAN_RAPPORT)));
                    leRapport.setRap_dateVisite(c.getString(c.getColumnIndex(KEY_DATEVISITE_RAPPORT)));
                    leRapport.setRap_dateRapport(c.getString(c.getColumnIndex(KEY_DATERAPPORT_RAPPORT)));
                    leRapport.setRap_motif(c.getString(c.getColumnIndex(KEY_MOTIF_RAPPORT)));
                    leRapport.setIdConfiance(c.getInt(c.getColumnIndex(KEY_CONFIANCE_RAPPORT)));
                    leRapport.setRap_etat(c.getString(c.getColumnIndex((KEY_ETAT_RAPPORT))));
                    leRapport.setLePraticien(lePraticien);




                lesRapports.add(leRapport);
                c.moveToNext();

                }


            c.close();


        return lesRapports;
    }



    public TypeConfiance getLaConfiance(int code) {


        TypeConfiance confiance = new TypeConfiance(0,"");

            Cursor c = db.rawQuery("select * from " + TABLE_TCONFIANCE +
                    " where " + KEY_IDCONFIANCE_TCONFIANCE + " = '"+code+"'", null);

            Log.i("GsbSQLiteManager", "requête");


            if (c.getCount() == 0) {
                confiance = null;
            } else {
                if (c.moveToFirst()) {
                    confiance.setIdConfiance(c.getInt(c.getColumnIndex(KEY_IDCONFIANCE_TCONFIANCE)));
                    confiance.setLibelleConfiance(c.getString(c.getColumnIndex(KEY_LIBELLE_TCONFIANCE)));
                    c.close();
                }
            }


        return confiance;
    }


    public TypePraticien getLeTypePraticien(String code) {


        TypePraticien type = new TypePraticien("","","");

        Cursor c = db.rawQuery("select * from " + TABLE_TPRATICIEN +
                " where " + KEY_CODE_TPRATICIEN+ " = '"+code+"'", null);

        Log.i("GsbSQLiteManager", "requête");


        if (c.getCount() == 0) {
            type = null;
        } else {
            if (c.moveToFirst()) {
                type.setTyp_code(c.getString(c.getColumnIndex(KEY_CODE_TPRATICIEN)));
                type.setTyp_libelle(c.getString(c.getColumnIndex(KEY_LIBELLE_TPRATICIEN)));
                type.setTyp_lieu(c.getString(c.getColumnIndex(KEY_LIEU_TPRATICIEN)));
                c.close();
            }
        }


        return type;
    }




    public ArrayList<Praticien> getLesPraticiens(String matricule) {
        ArrayList<Praticien> lesPraticiens = new ArrayList<Praticien>();
        int num = 0;
        Cursor c = db.rawQuery("SELECT * FROM praticien WHERE vis_matricule = '"+matricule+"'", null);
       if(c.moveToFirst()){
           while (!c.isAfterLast()) {
               Praticien lePraticien = new Praticien(0,"","","","","",0.0f,"","");

               lePraticien.setPra_num(c.getInt(c.getColumnIndex(KEY_NUMERO_PRATICIEN)));
               lePraticien.setPra_nom(c.getString(c.getColumnIndex(KEY_NOM_PRATICIEN)));
               lePraticien.setPra_prenom(c.getString(c.getColumnIndex(KEY_PRENOM_PRATICIEN)));
               lePraticien.setPra_adresse(c.getString(c.getColumnIndex(KEY_ADRESSE_PRATICIEN)));
               lePraticien.setPra_cp(c.getString(c.getColumnIndex(KEY_CP_PRATICIEN)));
               lePraticien.setPra_ville(c.getString(c.getColumnIndex(KEY_VILLE_PRATICIEN)));
               lePraticien.setPra_coefNotoriete(c.getFloat(c.getColumnIndex(KEY_NOTORIETE_PRATICIEN)));
               lePraticien.setTyp_code(c.getString(c.getColumnIndex(KEY_CODE_TPRATICIEN)));
               lePraticien.setVis_matricule(c.getString(c.getColumnIndex(KEY_VISITEURMATRICULE_PRATICIEN)));

               int numAffiche = lePraticien.getPra_num();

               System.out.println("RECUPERATION DES PRATICIENS"+numAffiche);
               lesPraticiens.add(lePraticien);

               c.moveToNext();
           }
       }



            c.close();

        System.out.println("DANS LE MANAGE LA LISTE DES PRATICIENS "+lesPraticiens.get(0).getPra_num()+" "+lesPraticiens.get(1).getPra_num());

        return lesPraticiens;
    }

    public String getLesPraticiens(Integer num) {
        Praticien lePraticien = new Praticien(0,"","","","","",0.0f,"","");
        String nom = null;
        Cursor c = db.rawQuery("SELECT * FROM praticien WHERE pra_num = '"+num+"'", null);
        if(c.moveToFirst()) {
            lePraticien.setPra_num(c.getInt(c.getColumnIndex(KEY_NUMERO_PRATICIEN)));
            lePraticien.setPra_nom(c.getString(c.getColumnIndex(KEY_NOM_PRATICIEN)));
            lePraticien.setPra_prenom(c.getString(c.getColumnIndex(KEY_PRENOM_PRATICIEN)));
            lePraticien.setPra_adresse(c.getString(c.getColumnIndex(KEY_ADRESSE_PRATICIEN)));
            lePraticien.setPra_cp(c.getString(c.getColumnIndex(KEY_CP_PRATICIEN)));
            lePraticien.setPra_ville(c.getString(c.getColumnIndex(KEY_VILLE_PRATICIEN)));
            lePraticien.setPra_coefNotoriete(c.getFloat(c.getColumnIndex(KEY_NOTORIETE_PRATICIEN)));
            lePraticien.setTyp_code(c.getString(c.getColumnIndex(KEY_CODE_TPRATICIEN)));
            lePraticien.setVis_matricule(c.getString(c.getColumnIndex(KEY_VISITEURMATRICULE_PRATICIEN)));

            nom = lePraticien.getPra_nom();
            c.close();
        }



        return nom;
    }


    public ArrayList<Integer> getLesConfiances() {
        ArrayList<Integer> lesPraticiens = new ArrayList<Integer>();
        Integer num = 0;

        Cursor c = db.rawQuery("SELECT * FROM type_confiance", null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            TypeConfiance laConfiance = new TypeConfiance (0,"");
            laConfiance.setIdConfiance(c.getInt(c.getColumnIndex(KEY_IDCONFIANCE_TCONFIANCE)));
            laConfiance.setLibelleConfiance(c.getString(c.getColumnIndex(KEY_LIBELLE_TCONFIANCE)));

            num = laConfiance.getIdConfiance();
            System.out.println("LE NUMERO :"+num);
            lesPraticiens.add(num);
            c.moveToNext();
        }
        c.close();

System.out.println("LES NUMEROS DE CONFIANCE : "+lesPraticiens.get(0)+" ,"+lesPraticiens.get(1)+" ,"+lesPraticiens.get(2));
        return lesPraticiens;
    }







    public String getLaConfiance(Integer num) {
        TypeConfiance laConfiance = new TypeConfiance(0,"");
        String nom = null;
        Cursor c = db.rawQuery("SELECT * FROM type_confiance WHERE idConfiance = '"+num+"'", null);
        if(c.moveToFirst()) {
            laConfiance.setIdConfiance(c.getInt(c.getColumnIndex(KEY_IDCONFIANCE_TCONFIANCE)));
            laConfiance.setLibelleConfiance(c.getString(c.getColumnIndex(KEY_LIBELLE_TCONFIANCE)));

            nom = laConfiance.getLibelleConfiance();
            c.close();
        }



        return nom;
    }

    public void addRapport(String vis_matricule,int pra_num,String dateVisite,int idConfiance,String motif,String bilan){


        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date= new Date();
        String dateRapport = dateFormat.format(date);

        motif = motif.replace("'","''"); // Permet d'éviter la génération d'une erreur lorsque le motif contient une simple quote
        bilan = bilan.replace("'","''");// Permet d'éviter la génération d'une erreur lorsque le bilan contient une simple quote

        Log.i("Saisi rapport2 : ",dateRapport);
       /* ContentValues contentValues = new ContentValues();
        contentValues.put("rap_num","");
        contentValues.put("vis_matricule",vis_matricule);
        contentValues.put("pra_num",pra_num);
        contentValues.put("rap_bilan",bilan);
        contentValues.put("rap_dateVisite",dateVisite);
        contentValues.put("rap_dateRapport",dateRapport);
        contentValues.put("rap_motif",motif);
        contentValues.put("idConfiance",idConfiance);
        contentValues.put("rap_etat","");

        return db.insert("rapport_visite", null, contentValues);*/
        String req;
        req = "Insert into rapport_visite (vis_matricule,pra_num,rap_bilan,rap_dateVisite,rap_dateRapport,rap_motif,idConfiance,rap_etat) values ('"+vis_matricule+"',"+pra_num+",'"+bilan+"','"+dateVisite+"','"+dateRapport+"','"+motif+"',"+idConfiance+",'Non lu')";
        System.out.println(req);
        SQLiteStatement stmt = db.compileStatement(req);
        long id = stmt.executeInsert();

        System.out.println("Le id inserer "+id);



    }



}// class GsbSQLiteManager

