package fr.sio.nb.appli_cr.metier;

public class TypeConfiance {

    private int idConfiance;
    private String libelleConfiance;

    public TypeConfiance(int idConfiance,String libelleConfiance){
        this.idConfiance = idConfiance;
        this.libelleConfiance = libelleConfiance;

    }

    public int getIdConfiance() {
        return idConfiance;
    }

    public void setIdConfiance(int idConfiance) {
        this.idConfiance = idConfiance;
    }

    public String getLibelleConfiance() {
        return libelleConfiance;
    }

    public void setLibelleConfiance(String libelleConfiance) {
        this.libelleConfiance = libelleConfiance;
    }


}
